package ru.nsu.g.avoroknov.hellomvc;

import ru.nsu.g.avoroknov.hellomvc.gui.MainFrame;
import ru.nsu.g.avoroknov.hellomvc.model.Calc;

public class Main {
    public static void main(String[] args) {
        var calc = new Calc();

        new MainFrame(calc);
    }
}
