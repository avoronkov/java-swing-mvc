package ru.nsu.g.avoroknov.hellomvc.gui;

import ru.nsu.g.avoroknov.hellomvc.model.Calc;

import javax.swing.*;


public class MainFrame extends JFrame {

    private Calc calc;

    public MainFrame(Calc calc){
        this.calc = calc;

        var arg1 = new JSpinner(new SpinnerNumberModel());
        arg1.setBounds(130, 10, 100, 40);
        add(arg1);

        var arg2 = new JSpinner(new SpinnerNumberModel());
        arg2.setBounds(130, 60, 100, 40);
        add(arg2);

        var label = new JLabel("0");
        label.setBounds(130, 110, 100, 40);
        add(label);

        var b = new JButton("Multiply (*)");//create button
        b.setBounds(130,200,100, 40);
        b.addActionListener(actionEvent -> {
            // This is actually controller (!)
            var x = (Integer)arg1.getValue();
            var y = (Integer)arg2.getValue();
            var result = this.calc.multiply(x, y);
            label.setText(Integer.toString(result));
        });

        add(b);//adding button on frame
        setSize(400,500);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
